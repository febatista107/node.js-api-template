import fs from 'fs';
import winston from 'winston';

if(!fs.existsSync('logs')) {
    fs.mkdirSync('logs');
}

const env = process.env.NODE_ENV;

module.exports = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: `logs/api.${env}.log`,
            maxsize: 1048576,
            maxFiles: 10,
            colorize: false
        })
    ]
});