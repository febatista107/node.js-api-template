module.exports = {
    database: 'test-<project-name>',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: '<project-name>.sqlite',
        operatorsAliases: false,
        logging: false,
        define: {
            underscored: true
        }
    },
    jwtSecret: 'some_test_secret',
    jwtSession: {
        session: false
    }
};