import logger from './logger';

module.exports = {
    database: '<project-name>',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: '<project-name>.sqlite',
        operatorsAliases: false,
        logging: sql => {
            logger.info(`[${new Date()}] ${sql}`);
        },
        define: {
            underscored: true
        }
    },
    jwtSecret: 'some_prod_secret',
    jwtSession: {
        session: false
    }
};