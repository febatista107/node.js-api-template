import logger from './logger';

module.exports = {
    database: 'dev-<project-name>',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: '<project-name>.sqlite',
        operatorsAliases: false,
        logging: sql => {
            logger.info(`[${new Date()}] ${sql}`);
        },
        define: {
            underscored: true
        }
    },
    jwtSecret: 'some-dev-secret',
    jwtSession: {
        session: false
    }
};