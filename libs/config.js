module.exports = app => {
    const env = process.env.NODE_ENV;
    return require(`./config.${env}.js`);
};