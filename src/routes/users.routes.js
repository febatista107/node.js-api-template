module.exports = app => {
    const User = app.db.models.User;

    app.route('/user')
        /**
         * @api {post} /users Creates new user
         * @apiGroup User
         * @apiParam {String} name Name
         * @apiParam {String} email Email
         * @apiParam {String} password Password
         * @apiParamExample {json} Input
         *      {
         *          "name": "User Name"
         *          "email": "user.name@provider.com",
         *          "password": "123456"
         *      }
         * @apiSuccess {Number} id Register id
         * @apiSuccess {String} name Name
         * @apiSuccess {String} email Email
         * @apiSuccess {String} password Encripted password
         * @apiSuccess {Date} updated_at Update date
         * @apiSuccess {Date} created_at Create date
         * @apiSuccessExample {json} Success
         *      HTTP/1.1 200 OK
         *      {
         *          "id": 1,
         *          "name": "User Name",
         *          "email": "user.name@provider.com",
         *          "password": "$2a$10$SK1B1",
         *          "updated_at": "2018-10-09T15:17:00.778Z",
         *          "created_at": "2018-10-09T15:17:00.778Z"
         *      }
         * @apiErrorExample {json} Registration error
         *      HTTP/1.1 412 Precondition Failed
         */
        .post((req, res) => {
            User.create(req.body)
                .then(result => {
                    res.json(result);
                })
                .catch(error => {
                    res.status(412).json({ msg: error.message });
                });
        });

    app.route('/user')
        .all(app.auth.authenticate())
        /**
         * @api {get} /user Returns authenticated user
         * @apiGroup User
         * @apiHeader {String} Authorization User token
         * @apiHeaderExample {json} Header
         *      { "Authorization": "Bearer xyz.abc.123.hgf" }
         * @apiSuccess {Number} id Register id
         * @apiSuccess {String} name Name
         * @apiSuccess {String} email Email
         * @apiSuccessExample {json} Success
         *      HTTP/1.1 200 OK
         *      {
         *          "id": 1,
         *          "name": "User Name",
         *          "email": "user.name@provider.com"
         *      }
         * @apiErrorExample {json} Query error
         *      HTTP/1.1 412 Precondition Failed
         */
        .get((req, res) => {
            User.findById(req.user.id, {
                attributes: ['id', 'name', 'email']
            })
            .then(result => {
                res.json(result);
            })
            .catch(error => {
                res.status(412).json({ msg: error.message });
            });
        })
        /**
         * @api {delete} /user Remove authenticated user
         * @apiGroup User
         * @apiHeader {String} Authorization User token
         * @apiHeaderExample {json} Header
         *      { "Authorization": "Bearer xyz.abc.123.hgf" }
         * @apiSuccessExample {json} Success
         *      HTTP/1.1 204 No Content
         * @apiErrorExample {json} Remove error
         *      HTTP/1.1 412 Precondition Failed
         */
        .delete((req, res) => {
            User.destroy({
                where: { id: req.user.id }
            })
            .then(result => {
                res.sendStatus(204);
            })
            .catch(error => {
                res.status(412).json({ msg: error.message });
            });
        });
};