import jwt from 'jwt-simple';

module.exports = app => {
    const config = app.libs.config;
    const User = app.db.models.User;

    /**
     * @api {post} /login Login
     * @apiGroup Authentication
     * @apiParam {String} email User email
     * @apiParam {String} password User Password
     * @apiParamExample {json} Input
     *      {
     *          "email": "user.name@provider.com",
     *          "password": "123456"
     *      }
     * @apiSuccess {String} token Authenticated user token
     * @apiSuccessExample {json} Success
     *      HTTP/1.1 200 OK
     *      { "token": "xyz.abc.123.hgf" }
     * @apiErrorExample {json} Authentication error
     *      HTTP/1.1 401 Unauthorized
     */
    app.post('/login', (req, res) => {
        if(req.body.email && req.body.password) {
            const email = req.body.email;
            const password = req.body.password;
            User.findOne({ where: { email } })
                .then(user => {
                    if(User.isPassword(user.password, password)) {
                        const payload = { id: user.id };
                        res.json({
                            token: jwt.encode(payload, config.jwtSecret)
                        });
                    } else {
                        res.sendStatus(401);
                    }
                })
                .catch(error => {
                    res.sendStatus(401);
                });
        } else {
            res.sendStatus(401);
        }
    });
};