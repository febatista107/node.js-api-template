import cluster from 'cluster';
import os from 'os';

const cpus = os.cpus();
if(cluster.isMaster) {
    cpus.forEach(() => {
        cluster.fork();
    });
    cluster.on('listening', worker => {
        console.log('Cluster %d connected', worker.process.pid);
    });
    cluster.on('disconnect', worker => {
        console.log('Cluster %d disconnected', worker.process.pid);
    });
    cluster.on('exit', worker => {
        console.log('Cluster %d is offline', worker.process.pid);
        cluster.fork();
    });
} else {
    require('./server.js');
}