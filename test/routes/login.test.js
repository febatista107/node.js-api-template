describe('Routes: Login', () => {
    const User  = app.db.models.User;
    describe('POST /login', () => {
        beforeEach(done => {
            User.destroy({ where: {} })
                .then(() => User.create({
                    name: 'Test User',
                    email: 'test.user@email.test',
                    password: '123456'
                }))
                .then(() => done());
        });
        describe('status 200', () => {
            it('Returns authenticated user token', done => {
                request.post('/login')
                    .send({
                        email: 'test.user@email.test',
                        password: '123456'
                    })
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.include.keys('token');
                        done(err);
                    });
            });
        });
        describe('status 401', () => {
            it('Throws error when password is incorrect', done => {
                request.post('/login')
                    .send({
                        email: 'test.user@email.test',
                        password: 'wrong_password'
                    })
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });
            });
            it('Throws error when email not exist', done => {
                request.post('/login')
                    .send({
                        email: 'wrong_email',
                        password: 'wrong_password'
                    })
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });
            });
            it('Throws error when email and password are blank', done => {
                request.post('/login')
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });
            });
        });
    });
});