import jwt from 'jwt-simple';

describe('Routes: Users', () => {
    const User = app.db.models.User;
    const jwtSecret = app.libs.config.jwtSecret;
    let token;
    beforeEach(done => {
        User.destroy({ where: {} })
            .then(() => User.create({
                name: 'Test User',
                email: 'test.user@email.test',
                password: '123456'
            }))
            .then(user => {
                token = jwt.encode({ id: user.id }, jwtSecret);
                done();
            });
    });
    describe('GET /user', () => {
        describe('status 200', () => {
            it('Returns an authenticated user', done => {
                request.get('/user')
                    .set('Authorization', `Bearer ${token}`)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.name).to.eql('Test User');
                        expect(res.body.email).to.eql('test.user@email.test');
                        done(err);
                    });
            });
        });
    });
    describe('DELETE /user', () => {
        describe('status 204', () => {
            it('Deletes an authenticated user', done => {
                request.delete('/user')
                    .set('Authorization', `Bearer ${token}`)
                    .expect(204)
                    .end((err, res) => {
                        done(err);
                    });
            });
        });
    });
    describe('POST /user', () => {
        describe('status 200', () => {
            it('Creates a new user', done => {
                request.post('/user')
                    .send({
                        name: 'New Test User',
                        email: 'new.test.user@email.test',
                        password: '123456'
                    })
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.name).to.eql('New Test User');
                        expect(res.body.email).to.eql('new.test.user@email.test');
                        done(err);
                    });
            });
        });
    });
});