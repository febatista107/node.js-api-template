describe('Routes: Status', () => {
    describe('GET /', () => {
        it('Returns the API status', done => {
            request.get('/')
                .expect(200)
                .end((err, res) => {
                    const expected = { status: '<Project-name> API is running!' };
                    expect(res.body).to.eql(expected);
                    done(err);
                });
        });
    });
});