# Node.js API Template

This project is a template for Node.js APIs, where we have a pre-configured project ready to go.

## Usage

First you need to clone this project into your favorite workspace. Than, remove the `.git` folder, to reset the local git. You can use the following commands.

```shellscript
rm -rf .git
git init
```

Later, if you want to save your project in your own repository, you need to add the URL of the repo into origin.

```shellscript
git remote add origin <your-repo-url>
```

Then, you need to rename the project. You can use the string replacement feature, of your favorite text editor.

You have two strings to replace, one for the places where the name should be in lowercase, and another for the places where it should be in camelcase.

Where the name is in lowercase, search and replace the "<project-name>" string. And, where the name is in camelcase, search and replace the "<Project-name>" string.

Obviously, you also have to rename the folder, but you already know how to do that.

That's it! Now you can start the development of your own API, based on this template. During this overview, I'll explain the basics about this template, like arquiteture, modules and configurations.

## Running the project

To run the application, first, you need to install all the dependencies of the project. But wait, you don't have to install one by one.

The Node.js have a package manager, called `npm`, or Node Package Manager. It is basically a CLI(Command Line Interface), which you can manage the project modules.

One of the `npm` commands is the `install`. By running this command, the manager install all the modules automatically, based on the dependency list on the `package.json` file.

```shellscript
npm install
```

Now, you can run the application, by typing `start` command, of the `npm`, and tapping `Enter`.

```shellscript
npm start
```

You should have the following message on your terminal.

```
<Project-name> API is running on port 3000
```

To test it, you can open your browser and type `localhost:3000` into the navigation bar. You probably gonna have the following response.

```json
{
  "status": "<Project-name> API is running!"
}
```

It works! Know, you are ready to run your application.

<!-- ## Modules

For this template, I use some modules(dependencies) -->